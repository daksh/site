# Animal Farm, book review (Note: obviously contains spoilers!)

PIG BROTHER IS WATCHING YOU!

"Animal Farm" is a novel written by George Orwell in 1945. The story is a satire of the Russian Revolution and the rise of Stalin's dictatorship. It is set on a farm in England and tells the story of a group of farm animals who rebel against their human farmer, hoping to create a society where the animals can be equal, free, and happy.

The novel begins with the animals living on Manor Farm, owned by Mr. Jones, who is cruel and neglectful. The animals, led by the pigs, decide to overthrow Jones and run the farm themselves. They are successful in their rebellion and rename the farm "Animals Farm."

The pigs, who are the most intelligent animals on the farm, take on the role of leaders and begin to implement rules and regulations for the other animals to follow. Initially, the farm is a success and the animals work together to make it prosper. However, as time goes on, the pigs become more and more corrupt and begin to abuse their power. They change the farm's flag and the commandment "All animals are equal, but some animals are more equal than others." This change in the farm's principles is a clear indication of the pigs' growing ambition for power and control.

As the pigs gain more power, they begin to adopt more human-like behaviors and habits, such as wearing clothes, sleeping in beds, and drinking alcohol. They also establish a strict hierarchy among themselves, with Napoleon, the leader of the pigs, at the top. The other animals, particularly the sheep, are brainwashed into blindly following Napoleon and his regime.

The pigs also begin to manipulate and control the other animals in order to maintain their power. They use propaganda and indoctrination to sway the other animals to their side, and they use fear and intimidation to silence any dissent. The other animals are forced to work harder and longer hours, and their living conditions deteriorate. They begin to realize that their lives have not improved at all since the rebellion, and that they are now living under a new form of oppression.

The novel also highlights the dangers of blindly following those in power, and the importance of constantly questioning those in authority. Boxer, the horse, is a prime example of this. He is a hardworking and loyal animal who blindly follows Napoleon and the other pigs. However, when he becomes injured and is no longer able to work, Napoleon sells him to a glue factory, despite Boxer's dedication and loyalty. This serves as a stark reminder that those in power will ultimately prioritize their own interests over the well-being of their followers.

The novel also explores the theme of the corrupting nature of power. The pigs, who start off as noble and idealistic leaders, become increasingly corrupt and oppressive as they gain more power. This is a commentary on how those in power can easily become corrupted by their own ambition and the desire for control.

In the end, the novel leaves the reader with a sense of hopelessness and despair. The animals' rebellion has failed, and they are left living under the oppressive rule of the pigs. The novel serves as a warning of the dangers of allowing any group or individual to have too much power and control. It is a powerful and thought-provoking novel that highlights the importance of always being vigilant and questioning those in authority. The message of "Animal Farm" is as relevant today as it was when it was first published, and it continues to be a powerful commentary on the dangers of totalitarian regimes and the corrupting nature of power.
If you have enjoyed this, you might want to read 1984, also by George Orwell after this.

## Quotes
Four legs good, two legs bad.

All animals are equal, but some animals are more equal than others.

The only good human being is a dead one.

Man serves the interests of no creature except himself.

The Commandments:
	Whatever goes upon two legs is an enemy.
	Whatever goes upon four legs, or has wings, is a friend.
	No animal shall wear clothes.
	No animal shall sleep in a bed.
	No animal shall drink alcohol.
	No animal shall kill any other animal.
	All animals are equal.

## Beasts Of England
Beasts of Englands is an anthem made by the pigs. It describes the upcoming day of rebelion. It goes as follows:


	Beasts of England, beasts of Ireland,
	Beasts of every land and clime,
	Hearken to my joyful tidings
	Of the golden future time.

	Soon or late the day is coming,
	Tyrant Man shall be o'erthrown,
	And the fruitful fields of England
	Shall be trod by beasts alone.

	Rings shall vanish from our noses,
	And the harness from our back,
	Bit and spur shall rust forever,
	Cruel whips shall no more crack.

	Riches more than mind can picture,
	Wheat and barley, oats and hay,
	Clover, beans, and mangel-wurzels,
	Shall be ours upon that day.

	Bright will shine the fields of England,
	Purer shall its water be,
	Sweeter yet shall blow its breezes
	On the day that sets us free.

	For that day we all must labour,
	Though we die before it break;
	Cows and horses, geese and turkeys,
	All must toils for freedom's sake.

	Beasts of England, beasts of Ireland,
	Beasts of every land and clime,
	Hearken well and spread my tidings
	Of the golden future time. ” 
